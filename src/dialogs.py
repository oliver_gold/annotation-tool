from PySide2.QtWidgets import QDialog
import gui.pick_label_dialog_ui as pick_label
import gui.sure_to_delete_dialog as sure_del
import gui.text_notification as tex_not
import gui.edit_dialog as edit
import gui.enter_user_id as enter_id

from PySide2.QtCore import Qt


class pick_label_dialog(QDialog):
    def __init__(self, model, ret, parent=None):
        super(pick_label_dialog, self).__init__()
        self.ui = pick_label.Ui_Dialog()
        self.ui.setupUi(self)
        self.__ret = ret

        self.__model = model
        self.__model.itemChanged.connect(self.__on_checked)
        self.ui.listView.setModel(self.__model)

        self.ui.pushButton.clicked.connect(self.__ret_text)

    def __ret_text(self):
        self.__ret.append(self.ui.lineEdit.text())
        self.accept()

    def __on_checked(self, item):
        if item.checkState():
            self.__ret.append(item)
            item.setCheckState(Qt.Unchecked)
            self.accept()


class question_dialog(QDialog):
    def __init__(self, text=None, parent=None):
        super(question_dialog, self).__init__()
        self.ui = sure_del.Ui_Dialog()
        self.ui.setupUi(self)
        if text is not None:
            self.ui.label.setText(text)


class text_notification(QDialog):
    def __init__(self, text, parent=None):
        super(text_notification, self).__init__()
        self.ui = tex_not.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.label.setText(text)
        self.ui.pushButton.clicked.connect(self.complete)

    def complete(self):
        self.reject()


class edit_dialog(QDialog):
    def __init__(self, mask, lm, colors, parent=None):
        super(edit_dialog, self).__init__()
        self.ui = edit.Ui_Dialog()
        self.ui.setupUi(self)

        self.mask = mask

        self.label_manager = lm
        self.labels = lm.get_labels()
        self.labels.itemChanged.connect(self.on_label_checked)
        self.ui.listView_labels.setModel(self.labels)
        lab = self.mask.get_label()

        if lab is not None:
            i = 0
            while self.labels.item(i):
                if lab.get_name() == self.labels.item(i).get_label().get_name():
                    self.labels.item(i).setCheckState(Qt.Checked)
                    self.checked_label = i
                    break
                i += 1

        self.colors = colors
        self.colors.itemChanged.connect(self.on_color_checked)
        self.ui.listView_colors.setModel(self.colors)
        col = self.mask.background()
        i = 0
        while self.colors.item(i):
            if col.color() == self.colors.item(i).background().color():
                self.colors.item(i).setCheckState(Qt.Checked)
                self.checked_color = i
                break
            i += 1

        self.ui.pushButton_create_label.clicked.connect(self.create_label)

        self.ui.pushButton_accept.clicked.connect(self.__okay)
        self.ui.pushButton_cancel.clicked.connect(self.reject)

    def on_label_checked(self, item):
        if item.checkState():
            i = 0
            while self.labels.item(i):
                if item.get_label().get_name() != self.labels.item(i).get_label().get_name():
                    if self.labels.item(i).checkState():
                        self.labels.item(i).setCheckState(Qt.Unchecked)
                        break
                i += 1

    def on_color_checked(self, col_it):
        if col_it.checkState():
            i = 0
            while self.colors.item(i):
                if self.colors.item(i).id_ != col_it.id_:
                    if self.colors.item(i).checkState():
                        self.colors.item(i).setCheckState(Qt.Unchecked)
                        break
                i += 1

    def create_label(self):
        self.label_manager.create_label_2(self.ui.lineEdit.text())
        self.ui.lineEdit.setText("")

    def __okay(self):
        i = 0
        while self.colors.item(i):
            if self.colors.item(i).checkState():
                self.mask.setBackground(self.colors.item(i).background())
                break
            i += 1

        i = 0
        while self.labels.item(i):
            if self.labels.item(i).checkState():
                self.mask.set_label(self.labels.item(i).get_label())
                self.mask.setText(str(self.mask.get_id()) + ":" + str(self.mask.get_label().get_name()))
                break
            i += 1

        self.accept()

    def __cancel(self):
        # TODO unselect all
        self.reject()


class enter_id_dialog(QDialog):
    def __init__(self, ret):
        super(enter_id_dialog, self).__init__()
        self.ui = enter_id.Ui_Dialog()
        self.ui.setupUi(self)

        self.ret = ret

        self.ui.pushButton.clicked.connect(self.ok)

    def ok(self):
        if self.ui.lineEdit.text() == "":
            dia = text_notification("please enter your id")
            dia.exec_()
            return

        self.ret.append(self.ui.lineEdit.text())
        self.accept()
