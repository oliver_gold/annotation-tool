from PySide2.QtGui import QColor, QStandardItemModel, QStandardItem


class color_item(QStandardItem):
    def __init__(self, color, id_, parent=None):
        QStandardItem.__init__(self, parent)
        self.setBackground(QColor(color[0], color[1], color[2]))
        self.setCheckable(True)
        self.setEditable(False)
        self.id_ = id_

    def __eq__(self, other):
        if self.id_ == other.id_:
            return True
        return False


class colors():
    def __init__(self):
        self.__colors = [
            [78, 121, 167],
            [89, 161, 79],
            [156, 117, 95],
            [242, 142, 43],
            [237, 201, 72],
            [186, 176, 172],
            [225, 87, 89],
            [176, 122, 161],
            [118, 183, 178],
            [255, 157, 167]
        ]
        self.model = QStandardItemModel()
        id_ = 0
        for color in self.__colors:
            self.model.appendRow(color_item(color, id_))
            id_ += 1
        self.__ptr = 0

    def get_color(self):
        ret = self.__colors[self.__ptr]
        ret = QColor(ret[0], ret[1], ret[2])
        ret = self.model.item(self.__ptr).background()
        self.__ptr += 1
        if self.__ptr == 10:
            self.__ptr = 0
        return ret

    def get_color_model(self):
        return self.model

