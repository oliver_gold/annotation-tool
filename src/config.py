
class config():
    def __init__(self, config_file=None):
        """
        paths
        """
        self.__base_path = ""
        self.__data_path = self.__base_path + "data/input_data/x1/"

        self.__last_path = self.__data_path

        self.__working_index_path = self.__base_path + "data/working/indexes/"
        self.__working_image_path = self.__base_path + "data/working/images/"

        self.__use_working_dir = True
        self.__result_path = self.__base_path + "data/results/"
        self.__unet_path = self.__base_path + "sample_methods/membrane_unet/"
        self.__icon_path = self.__base_path + "gui/icons/"
        # TODO reset working dir and pointers when changing directory
        # TODO setting base path must change all other

        """
        image size, the images are scaled to
        """
        self.__width = 1600
        self.__height = 900

        """
        number of image to preload
        """
        self.__preload = 2

        """
        loading pointers; don't edit in final version
        for now: keep active pointer <= preloaded
        """
        self.__active_pointer = 1
        self.__preloaded = 1

        self.__config_file = None
        self.__config_file_loc = "configuration.txt"
        self.__load_from_file()

    def __load_from_file(self):
        with open(self.__config_file_loc, "r") as f:
            self.__config_file = f.readlines()
        for line in range(0, len(self.__config_file)):
            words = self.__config_file[line].split()
            if words[0] == "#":
                continue
            elif words[0] == "base_path":
                self.__base_path = words[1]
            elif words[0] == "data_path":
                self.__data_path = words[1]
            elif words[0] == "working_index_path":
                self.__working_index_path = words[1]
            elif words[0] == "working_image_path":
                self.__working_image_path = words[1]
            elif words[0] == "result_path":
                self.__result_path = words[1]
            elif words[0] == "unet_path":
                self.__unet_path = words[1]
            elif words[0] == "icon_path":
                self.__icon_path = words[1]

            elif words[0] == "width":
                self.__width = int(words[1])
            elif words[0] == "height":
                self.__height = int(words[1])

            elif words[0] == "preload":
                self.__preload = int(words[1])

            elif words[0] == "last_path":
                self.__last_path = words[1]
            elif words[0] == "active_pointer":
                self.__active_pointer = int(words[1])
            elif words[0] == "preloaded":
                self.__preloaded = int(words[1])

        if self.__last_path != self.__data_path:
            self.__reset_dir()

    def data_path(self):
        return self.__data_path

    def working_index_path(self):
        return self.__working_index_path

    def working_image_path(self):
        return self.__working_image_path

    def result_path(self):
        return self.__result_path

    def unet_path(self):
        return self.__unet_path

    def uses_working_dir(self):
        return self.__use_working_dir

    def resolution(self):
        return self.__height, self.__width

    def width(self):
        return self.__width

    def height(self):
        return self.__height

    def preload(self):
        return self.__preload

    def active_pointer(self):
        return self.__active_pointer

    def increment_active_pointer(self):
        self.__active_pointer += 1
        self.__edit_file("active_pointer", str(self.__active_pointer))

    def decrement_active_pointer(self):
        self.__active_pointer -= 1
        self.__edit_file("active_pointer", str(self.__active_pointer))

    def increment_preloaded_pointer(self):
        self.__preloaded += 1
        self.__edit_file("preloaded", str(self.__preloaded))

    def __edit_file(self, key, value):
        for line in range(0, len(self.__config_file)):
            words = self.__config_file[line].split()
            if words[0] == key:
                self.__config_file[line] = key + " " + value + "\n"
        with open(self.__config_file_loc, "w") as f:
            f.writelines(self.__config_file)

    def preloaded_pointer(self):
        return self.__preloaded

    def icon_path(self):
        return self.__icon_path

    def __reset_dir(self):
        # TODO file cleanup
        self.__active_pointer = 0
        self.__preloaded = 0
        self.__edit_file("last_path", self.__data_path)
