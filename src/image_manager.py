import threading

from os import listdir, remove
from os.path import isfile, join

from skimage import io, util, transform
from skimage.morphology import binary_dilation

import time

from src.mask_item import mask_item, id_manager
from sample_methods.superpixel import calculate_superpixel
from sample_methods.kibo_net import inv_kibo
from sample_methods.membrane_unet import inv_unet

from PySide2.QtGui import QColor
from PySide2.QtWidgets import QProgressDialog
from PySide2.QtCore import Qt

import numpy as np

from src import image
from sample_methods.kibo_net.reverse_dist_field import convertImage
from src.dialogs import text_notification


def show_working_dialog(dia, flag):
    dia.append(text_notification("working, please wait!"))
    dia[0].show()
    while flag[0]:
        pass


class wait_thread(threading.Thread):
        def __init__(self, func, dia, flag):
            threading.Thread.__init__(self)
            self.daemon = True
            self.runnable = func
            self.dia = dia
            self.flag = flag

        def run(self):
            self.runnable(self.dia, self.flag)


class image_manager():
    def __init__(self, conf, colors, label_manager, methods, idm):
        self.__config = conf
        self.__colors = colors
        self.__label_manager = label_manager
        if self.__config.preloaded_pointer() == 0:
            self.__id_manager = id_manager()
            idm.append(self.__id_manager)

        self.__thread_pointers = [self.__config.preloaded_pointer(), self.__config.active_pointer() + self.__config.preload()]
        self.__thread = None

        self.__methods = methods  # TODO
        self.__file_names = [f for f in listdir(self.__config.data_path()) if isfile(join(self.__config.data_path(), f))]
        self.__file_names.sort()

        self.__imgs = np.full(len(self.__file_names), None, dtype=object)
        self.__mods = np.full(len(self.__file_names), None, dtype=object)

    def get_file_name(self, x):
        return self.__file_names[x]

    def get_image_number(self):
        return len(self.__file_names)

    def __wait(self, offset=0):
        while self.__mods[self.__config.active_pointer() + offset] is None:
            time.sleep(.5)
        while len(self.__mods[self.__config.active_pointer() + offset]) < 1:
            time.sleep(.5)

    def __load_from_disk(self, x):
        self.__imgs[x] = load_img_rgb(self.__config.data_path() + self.__file_names[x], self.__config.resolution())
        mods = []
        with open(self.__config.working_index_path() + str(x) + ".txt", "r") as f:
            mask_images = f.readlines()
        ret = 0
        for line in range(0, len(mask_images)):
            words = mask_images[line].split()

            label = None
            if len(words) == 4:
                label = self.__label_manager.get_label(words[3])

            m = mask_item(int(words[0]), label)
            m.setText(words[2])
            if int(words[0]) > ret:
                ret = int(words[0])

            save_path = self.__config.working_image_path() + words[0]
            m.set_save_path(save_path)

            if words[1] == "BINARY":  # or words[1] == "POLYGON" or words[1] == "POLY_LINE":
                m.set_binary_mask(np.load(save_path + ".npy"))
            elif words[1] == "NET_OUTPUT":
                m.set_binary_mask(np.load(save_path + ".npy"), True)
            elif words[1] == "POLYGON":
                points = np.load(save_path + ".npy")
                m.set_polygon_mask(points[0], points[1])
            elif words[1] == "POLY_LINE":
                points = np.load(save_path + ".npy")
                m.set_poly_line_mask(points[0], points[1])
            elif words[1] == "SEGMENT":
                m.set_segment_mask(np.load(save_path + ".npy"))
            elif words[1] == "PIXEL":
                m.set_pixel_mask(np.load(save_path + ".npy"))
                print("pixel is deprecated and should be removed")
            else:
                print("found unknown mask!")
                print(words[1])
                continue
            m.setBackground(self.__colors.get_color())
            mods.append(m)
        self.__mods[x] = mods
        return ret

    def fetch_image_startup(self, idm):
        if idm is not None:
            print("fetching next image")
            return self.fetch_next_image()
        else:
            if self.__config.active_pointer():
                self.__config.decrement_active_pointer()
            i = 0
            for x in range(0, self.__config.preloaded_pointer()):
                id_ = self.__load_from_disk(x)
                if id_ > i:
                    i = id_
            self.__id_manager = id_manager(i)
            ret = [self.__imgs[self.__config.active_pointer()], self.__mods[self.__config.active_pointer()]]
            self.__config.increment_active_pointer()
            return ret

    def fetch_next_image(self, masks=None):
        """
        returns the next image and its corresponding masks
        :param masks: masks from the current image to be saved
        :return: [image, [masks]]
        """
        self.__wait()
        """
        save masks of the current image
        """
        if masks is not None:
            self.__mods[self.__config.active_pointer() - 1] = masks
            print("saved masks to masks[" + str(self.__config.active_pointer() - 1) + "]")

        """
        load image and corresponding masks to return
        """
        tmp = []
        tmp.append(self.__imgs[self.__config.active_pointer()])
        tmp.append(self.__mods[self.__config.active_pointer()])

        self.__config.increment_active_pointer()

        """
        preload another image 
        """
        if self.__config.active_pointer() + self.__config.preload() > self.__thread_pointers[1]:
            self.__thread_pointers[1] += 1
            self.preload_images()
        return tmp

    def fetch_prev_image(self, masks=None):
        if masks is not None:
            self.__mods[self.__config.active_pointer() - 1] = masks
            print("saved masks to masks[" + str(self.__config.active_pointer() - 1) + "]")

        tmp = []
        tmp.append(self.__imgs[self.__config.active_pointer() - 2])
        tmp.append(self.__mods[self.__config.active_pointer() - 2])

        self.__config.decrement_active_pointer()
        return tmp

    def preload_images(self):
        """
        calls a thread that loads images and processes activated methods on it
        :return: True if thread was started, False if thread was already running
        """
        if self.__thread is not None:
            if self.__thread.is_alive():
                return False

        self.__thread = image_loader(work, self.__config, self.__colors, self.__file_names,
                                     self.__imgs, self.__mods, self.__methods,
                                     self.__thread_pointers, self.__id_manager)
        self.__thread.start()
        return True

    def get_id_manager(self):
        return self.__id_manager


def gray_to_rgb(im):
    """
    helper function to convert grayscale images to corresponding gray images in rgb format
    :param im: grayscale image to convert
    :return: rgb representation of input image
    """
    w, h = im.shape
    ret = np.empty((w, h, 3), dtype=np.uint8)
    ret[:, :, 2] = ret[:, :, 1] = ret[:, :, 0] = im
    return ret


def load_img_rgb(path, resolution):
    """
    reads an image from disk and makes sure it is in rgb format without influence of alpha channel
    :param path: path to the image
    :param resolution: desired resolution TODO only resize if one dimension bigger than 1600x900
    :return: loaded image as numpy array
    """
    img = io.imread(path)
    if img.ndim == 2:
        img = gray_to_rgb(img)
    elif len(img[0][0]) == 4:
        img = image.load_img(path, color_mode="rgb")
        img = np.asanyarray(img)
    img = transform.resize(img, resolution)
    img = util.img_as_ubyte(img)
    return img


def work(config, colors, file_names, imgs, mods, methods, thread_pointers, id_manager):
    """
    applies all methods for all images to preload
    only to be called from preload thread
    :param file_names: array with all file names contained in config.data_path()
    :param images: array that images are meant to store in
    :param models: array that corresponding masks are meant to store in
    :param methods: TODO all methods that are meant to be processed
    :param thread_pointers: two int pointers to control the loop
    :return: nothing, since run as a thread
    """
    path = config.data_path()
    while thread_pointers[0] < thread_pointers[1]:
        #print("TP:" + str(thread_pointers[0]) + "|" + str(thread_pointers[1]))
        loading = thread_pointers[0]
        p = path + file_names[loading]
        read_img = load_img_rgb(p, config.resolution())
        imgs[loading] = read_img

        """
        superpixel 500
        """
        m = mask_item(id_manager.get_id())
        calculate_superpixel(imgs[loading], m, colors, 500)
        m.setText(file_names[loading] + str(m.text()))
        m.save(config.working_image_path())

        mods[loading] = [m]
        print("done processing 500 spx of image " + str(loading))
        """
        unet output
        """
        """mm = mask_item(id_manager.get_id())

        # predicted = read_img[:, :, 0]
        # predicted = run_unet(config.unet_path(), predicted)
        predicted = inv_unet.wrap_invocation(read_img)

        predicted = transform.resize(predicted, config.resolution())
        predicted = util.img_as_ubyte(predicted)

        pred_mask = predicted < 50

        mm.set_binary_mask(pred_mask, True)
        mm.setBackground(colors.get_color())
        mm.setForeground(QColor(0, 0, 0))
        mm.setText(file_names[loading] + "unet_output")
        mm.save(config.working_image_path())

        mods[loading].append(mm)"""

        """
        unet output segments
        """
        """edges = post_process_unet(pred_mask)
        e = mask_item()
        e.set_segment_mask(edges)
        e.setBackground(colors.get_color())
        e.setForeground(QColor(0, 0, 0))
        e.setText(file_names[loading] + "edges")
        e.save(config.working_image_path())

        mods[loading].append(e)"""

        """
        kibo net
        """
        """net_out = inv_kibo.wrap_invocation(config.data_path(), file_names[loading])
        skel = convertImage(net_out, config)
        skel = binary_dilation(skel)
        skel = binary_dilation(skel)
        skel = binary_dilation(skel)

        s = mask_item(id_manager.get_id())
        s.set_binary_mask(skel, True)
        s.setBackground(colors.get_color())
        s.setText("KiBo-Net-result-" + file_names[loading])

        s.save(config.working_image_path())
        #mods[loading].append(s)
        mods[loading] = [s]"""

        print("done processing image " + str(loading))


        """
        saving to txt
        """
        save_to_txt(config.working_index_path(), str(loading), m)
        # save_to_txt(config.working_index_path(), str(loading), mm)
        #save_to_txt(config.working_index_path(), str(loading), s)

        thread_pointers[0] += 1
        config.increment_preloaded_pointer()


def save_label_txt(index_path, ptr, mask):
    with open(index_path + str(ptr) + ".txt", "r") as f:
        lines = f.readlines()
    with open(index_path + str(ptr) + ".txt", "w") as f:
        for line in lines:
            words = line.split()
            if int(words[0]) != mask.get_id():
                f.write(line)
            else:
                l = str(mask.get_id()) + " " + mask.get_mask_type().name + " " + mask.text() + " " + mask.get_label().get_name()
                f.write(l + "\n")


def save_to_txt(index_path, ptr, mask):
    with open(index_path + str(ptr) + ".txt", "a") as f:
        # f.write(mask.text() + " " + mask.get_mask_type().name + "\n")
        line = str(mask.get_id()) + " " + mask.get_mask_type().name + " " + mask.text()
        if mask.get_label() is not None:
            line = line + " " + mask.get_label().get_name()
        f.write(line + "\n")


def remove_mask(index_path, ptr, mask):
    with open(index_path + str(ptr) + ".txt", "r") as f:
        lines = f.readlines()
    with open(index_path + str(ptr) + ".txt", "w") as f:
        for line in lines:
            words = line.split()
            if int(words[0]) != mask.get_id():
                f.write(line)
            else:
                remove(mask.get_save_path() + ".npy")


class image_loader(threading.Thread):
    """
    simple thread inheritance
    just calls the work function
    """
    def __init__(self, function_that_loads, conf, colors, file_names,
                 imgs, mods, methods, thread_pointers, id_manager):
        threading.Thread.__init__(self)
        self.daemon = True
        self.runnable = function_that_loads
        self.config = conf
        self.colors = colors
        self.file_names = file_names
        self.imgs = imgs
        self.mods = mods
        self.methods = methods
        self.thread_pointers = thread_pointers
        self.id_manager = id_manager

    def run(self):
        self.runnable(self. config, self.colors, self.file_names, self.imgs,
                      self.mods, self.methods, self.thread_pointers, self.id_manager)
