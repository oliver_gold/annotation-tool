from PySide2.QtGui import QStandardItem


class label():
    def __init__(self, name):
        self.__name = name

    def get_name(self):
        return self.__name

    def get_string(self):
        return self.__name


class label_item(QStandardItem):
    def __init__(self, text, parent=None):
        QStandardItem.__init__(self, parent)
        self.setEditable(False)
        self.setCheckable(True)
        self.setText(text)
        self.__counter = 1

    def get_label(self):
        l = label(self.text())
        self.__increment_counter()
        return l

    def __increment_counter(self):
        self.__counter += 1

    def __eq__(self, other):
        if self.text() == other.text():
            return True
        return False
