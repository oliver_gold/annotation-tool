from PySide2.QtWidgets import QDialog
from PySide2.QtGui import QStandardItem
from PySide2.QtCore import Qt
from skimage import io

import numpy as np
from copy import deepcopy

from skimage.draw import polygon, line
from skimage.morphology import binary_dilation

from enum import IntEnum

from src.dialogs import text_notification


class mask_type(IntEnum):
    NONE = 0
    SEGMENT = 1
    BINARY = 2
    PIXEL = 3
    POLYGON = 4
    POLY_LINE = 5

    NET_OUTPUT = 6


class id_manager():
    def __init__(self, highest_loaded_id=None):
        self.__id = 0
        if highest_loaded_id is not None:
            self.__id = highest_loaded_id + 1

    def get_id(self):
        ret = self.__id
        self.__id += 1
        return ret


class mask_item(QStandardItem):
    """
    TODO derivates
    Stores a mask and additional information for one label
    A mask can only contain superpixel segments OR a pixelmask
    """

    # TODO to add new method add [flag, getter, setter, checks in others]
    def __init__(self, id_, label=None, parent=None, active_bin_seg=False):
        QStandardItem.__init__(self, parent)
        self.__id = id_
        self.setText(str(id_))

        self.setCheckable(True)
        self.setEditable(False)

        self.__mask_type = mask_type.NONE
        self.__mask = None
        self.__mask_vertices = None

        self.__segments = None
        self.__active_segments = []

        self.__polygon_points = []

        self.__poly_line_points = []

        self.__save_path = None

        self.is_active_bin_seg = active_bin_seg

        self.__label = None
        if label is not None:
            self.set_label(label)

    def save(self, path):
        """
        TODO polygon mask
        """
        # self.__save_path = path + str(self.text())
        self.__save_path = path + str(self.__id)
        if self.__mask_type is mask_type.SEGMENT:
            np.save(self.__save_path, self.__segments, False, False)
        elif self.__mask_type == mask_type.POLYGON:
            np.save(self.__save_path, self.__polygon_points, False, False)
        elif self.__mask_type == mask_type.POLY_LINE:
            np.save(self.__save_path, self.__poly_line_points, False, False)
        elif self.__mask_type > mask_type.SEGMENT:
            np.save(self.__save_path, self.__mask, False, False)
        else:
            print("tried to save an empty mask item " + str(self.__mask_type))
            dia = text_notification("tried to save an empty mask item " + str(self.__mask_type))
            dia.exec_()

    """
    setter methods
    """
    def set_save_path(self, s):
        self.__save_path = s

    def set_active_segments_mask(self, m):
        if self.__mask_type is mask_type.SEGMENT:
            self.__mask = m
            return True
        return False

    def set_binary_mask(self, m, net_output=False):
        if self.__mask_type is not mask_type.NONE:
            return False
        self.__mask = m
        if net_output:
            self.__mask_type = mask_type.NET_OUTPUT
        else:
            self.__mask_type = mask_type.BINARY
        return True

    def set_polygon_mask(self, polygon_points_x=[], polygon_points_y=[], resolution=(900, 1600)):
        if self.__mask_type is not mask_type.NONE:
            return False
        self.__polygon_points.append(polygon_points_x)
        self.__polygon_points.append(polygon_points_y)
        self.__mask = np.zeros(resolution, dtype=bool)
        self.__mask_type = mask_type.POLYGON
        self.compute_polygon_mask()
        return True

    def set_poly_line_mask(self, poly_line_points_x=[], poly_line_points_y=[], resolution=(900, 1600)):
        if self.__mask_type is not mask_type.NONE:
            return False
        self.__poly_line_points.append(poly_line_points_x)
        self.__poly_line_points.append(poly_line_points_y)
        self.__mask = np.zeros(resolution, dtype=bool)
        self.__mask_type = mask_type.POLY_LINE
        self.compute_poly_line_mask()
        return True

    def set_pixel_mask(self, m):
        if self.__mask_type is not mask_type.NONE:
            return False
        self.__mask = m
        self.__mask_type = mask_type.PIXEL
        return True

    def set_segment_mask(self, seg):
        if self.__mask_type is not mask_type.NONE:
            return False
        self.__segments = seg
        self.__mask_type = mask_type.SEGMENT
        return True

    def set_label(self, label):
        ret = self.__label
        self.__label = label
        self.setText(self.text() + ":" + self.__label.get_string())

        return ret

    """
    getter methods
    """
    def get_id(self):
        return self.__id

    def get_label(self):
        return self.__label

    def get_mask(self):
        if self.__mask_type > mask_type.SEGMENT:
            return self.__mask
        return False

    def get_active_segments_mask(self):
        if self.__mask_type is mask_type.SEGMENT:
            return self.__mask
        return False

    def get_segments(self):
        return self.__segments

    def get_segment(self, y, x):
        return self.__segments[y, x]

    def get_active_segments(self):
        return self.__active_segments

    def get_polygon_points(self):
        return self.__polygon_points

    def get_poly_line_points(self):
        return self.__poly_line_points

    def get_mask_type(self):
        return self.__mask_type

    def get_save_path(self):
        return self.__save_path

    def get_mask_w_vertices(self):
        if self.__mask_type is not mask_type.POLYGON and self.__mask_type is not mask_type.POLY_LINE:
            return None
        return self.__mask_vertices

    """
    checker methods
    """
    def is_pixel_mask(self):
        return self.__mask_type is mask_type.PIXEL

    def is_polygon_mask(self):
        return self.__mask_type is mask_type.POLYGON

    def is_pol_line_mask(self):
        return self.__mask_type is mask_type.POLY_LINE

    def is_binary_mask(self):
        return self.__mask_type is mask_type.BINARY

    def is_segment_mask(self):
        return self.__mask_type is mask_type.SEGMENT

    """
    misc
    """
    def overwrite_mask(self, m):
        if self.__mask_type is not mask_type.NET_OUTPUT and self.__mask_type is not mask_type.BINARY:
            return False
        self.__mask = m
        return True

    def empty_active_segments(self):
        self.__active_segments = []

    def __eq__(self, other):
        if id(self) == id(other):
            return True
        return False

    def add_polygon_point(self, x, y):
        self.__polygon_points[0].append(x)
        self.__polygon_points[1].append(y)
        return True

    def add_poly_line_point(self, x, y):
        self.__poly_line_points[0].append(x)
        self.__poly_line_points[1].append(y)
        return True

    def compute_polygon_mask(self):
        if self.__mask_type is not mask_type.POLYGON:
            return False
        self.__mask = np.zeros(self.__mask.shape, dtype=bool)
        rr, cc = polygon(self.__polygon_points[1], self.__polygon_points[0])
        self.__mask[rr, cc] = True

        self.__mask_vertices = deepcopy(self.__mask)
        rad = 4
        for p in range(0, len(self.__polygon_points[0])):
            for row in range(self.__polygon_points[1][p] - rad, self.__polygon_points[1][p] + rad + 1):
                if row < 0 or row >= self.__mask.shape[0]:
                    continue
                for col in range(self.__polygon_points[0][p] - rad, self.__polygon_points[0][p] + rad + 1):
                    if col < 0 or col >= self.__mask.shape[1]:
                        continue
                    self.__mask_vertices[row, col] = True
        return True

    def compute_poly_line_mask(self):
        if self.__mask_type is not mask_type.POLY_LINE:
            return False
        self.__mask = np.zeros(self.__mask.shape, dtype=bool)

        if len(self.__poly_line_points[0]) > 1:
            for i in range (1, len(self.__poly_line_points[0])):
                rr, cc = line(self.__poly_line_points[1][i-1], self.__poly_line_points[0][i-1],
                              self.__poly_line_points[1][i], self.__poly_line_points[0][i])
                self.__mask[rr, cc] = True
        self.__mask = binary_dilation(self.__mask)
        self.__mask = binary_dilation(self.__mask)
        self.__mask = binary_dilation(self.__mask)

        self.__mask_vertices = deepcopy(self.__mask)
        rad = 4
        for p in range(0, len(self.__poly_line_points[0])):
            for row in range(self.__poly_line_points[1][p] - rad, self.__poly_line_points[1][p] + rad + 1):
                if row < 0 or row >= self.__mask.shape[0]:
                    continue
                for col in range(self.__poly_line_points[0][p] - rad, self.__poly_line_points[0][p] + rad + 1):
                    if col < 0 or col >= self.__mask.shape[1]:
                        continue
                    self.__mask_vertices[row, col] = True

        return True
