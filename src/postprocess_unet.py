from skimage.morphology import erosion, flood
from skimage.util import img_as_ubyte
import numpy as np


def post_process_unet(bool_mask):
    """
    generate 0, 255 -only edge mask
    """
    binary_edges = np.bitwise_xor(bool_mask, erosion(bool_mask))
    edges = np.zeros(binary_edges.shape, dtype=np.uint8)
    edges[binary_edges] = 255
    """
    grayscale segment image and mask that keeps track of processed pixels
    """
    processed_map = np.zeros(binary_edges.shape, dtype=bool)
    segmented = np.zeros(processed_map.shape, dtype=np.int)
    rows, cols = processed_map.shape
    label = 0
    for r in range(rows):
        for c in range(cols):
            if not processed_map[r, c]:
                mask = flood(edges, (r, c), connectivity=1)
                segmented[mask] = label
                processed_map = np.bitwise_or(processed_map, mask)
                label += 1
    return segmented
    rgb_mask = np.zeros((rows, cols, 3), dtype=np.uint8)
    c = np.uint8(255/label)
    for i in range(label):
        color = [c*i, 255-c*i, c*i]
        m = segmented == i
        rgb_mask[m] = color

    return rgb_mask
