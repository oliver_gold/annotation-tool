
import numpy as np
import qimage2ndarray as i2a

from skimage.segmentation import mark_boundaries
from skimage import util

from copy import deepcopy

from PySide2.QtCore import Qt

from src.mask_item import mask_type


class canvas():
    def __init__(self, img, res=(900, 1600)):
        self.__height, self.__width = res
        self.__image = img
        self.__superpixel_mask_item = None
        self.__overlayed_superpixels = None
        self.__mask_items = []
        self.__sum_pixel_mask = None
        self.__result_image = None

    def clear(self):
        self.__image = None
        self.__superpixel_mask_item = None
        self.__overlayed_superpixels = None
        self.__mask_items = []
        self.__sum_pixel_mask = None
        self.__result_image = None

    def update_image(self, image):
        self.__image = util.img_as_ubyte(image)

    def get_superpixel_mask_item(self):
        return self.__superpixel_mask_item

    def superpixels_active(self):
        if not self.__superpixel_mask_item:
            return False
        return True

    def register_mask(self, mask):
        if mask.is_segment_mask():
            """make sure only one superpixel mask is active at once"""
            if self.__superpixel_mask_item is not None:
                self.__superpixel_mask_item.setCheckState(Qt.Unchecked)
            self.__superpixel_mask_item = mask
        elif mask.get_mask_type() > mask_type.SEGMENT:
            if mask not in self.__mask_items:
                self.__mask_items.append(mask)

        self.__compute_result_image()

    def unregister_mask(self, mask):
        if mask.is_segment_mask():
            self.__superpixel_mask_item = None
        elif mask.get_mask_type() > mask_type.SEGMENT:
            self.__mask_items.remove(mask)

        self.__compute_result_image()

    def __overlay_superpixels(self):
        self.__overlayed_superpixels = util.img_as_ubyte(
            mark_boundaries(self.__image, self.__superpixel_mask_item.get_segments()))

    def __overlay_active_superpixels(self):
        m = self.__superpixel_mask_item.get_active_segments_mask()
        if m is None:
            return False
        self.__result_image = util.img_as_ubyte(self.__result_image)
        col = self.__superpixel_mask_item.background().color()
        col = np.array([col.red(), col.green(), col.blue()])
        self.__result_image = util.img_as_float64(self.__result_image)
        self.__result_image[np.nonzero(m)] *= col / 255  # = [255, 200, 100]
        self.__result_image = util.img_as_ubyte(self.__result_image)
        return True

    def __compute_result_image(self):
        if self.__superpixel_mask_item:
            self.__overlay_superpixels()
            self.__result_image = deepcopy(self.__overlayed_superpixels)
        else:
            self.__result_image = deepcopy(self.__image)

        active_bin_segs = None

        for i in self.__mask_items:
            if i.get_mask_type() == mask_type.POLYGON:
                continue
            if i.is_active_bin_seg:
                active_bin_segs = i
                continue
            col = i.background().color()
            col = np.array([col.red(), col.green(), col.blue()])
            if i.get_mask_type() == mask_type.POLY_LINE:
                n = i.get_mask_w_vertices()
            else:
                n = i.get_mask()
            self.__result_image[n] = col

        self.__result_image = util.img_as_float64(self.__result_image)
        for i in self.__mask_items:
            if i.get_mask_type() != mask_type.POLYGON:
                continue
            col = i.background().color()
            col = np.array([col.red(), col.green(), col.blue()])
            self.__result_image[i.get_mask_w_vertices()] *= col / 255
        self.__result_image = util.img_as_ubyte(self.__result_image)

        if self.__superpixel_mask_item:
            if self.__superpixel_mask_item.get_active_segments_mask is not None:
                self.__overlay_active_superpixels()

        if active_bin_segs is not None:
            i = active_bin_segs
            col = i.background().color()
            col = np.array([col.red(), col.green(), col.blue()])
            n = i.get_mask()
            self.__result_image[n] = col

    def recompute(self):
        self.__compute_result_image()

    def get_result_image(self):
        if self.__result_image is not None:
            return i2a.array2qimage(self.__result_image, True)
        return None

    def update_superpixel_segments(self, x, y, empty=False):
        mask = np.zeros((self.__height, self.__width), dtype=bool)
        if empty:
            self.__superpixel_mask_item.empty_active_segments()
        else:
            # fetch segment number
            segment = self.__superpixel_mask_item.get_segment(y, x)
            if segment in self.__superpixel_mask_item.get_active_segments():
                self.__superpixel_mask_item.get_active_segments().remove(segment)
            else:
                self.__superpixel_mask_item.get_active_segments().append(segment)

            for i in self.__superpixel_mask_item.get_active_segments():
                mask += self.__superpixel_mask_item.get_segments() == i

        self.__superpixel_mask_item.set_active_segments_mask(mask)

        self.__compute_result_image()
        return True

