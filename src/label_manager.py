from PySide2.QtGui import QStandardItemModel
from PySide2.QtCore import Qt

from src.label_item import label_item
from src.dialogs import pick_label_dialog
from os.path import exists


class label_manager():
    def __init__(self, config):
        self.__config = config
        self.__unlabeled = label_item("unlabeled")
        self.__labels = QStandardItemModel()

        label_text = self.__config.working_index_path() + "labels.txt"
        if not exists(label_text):
            with open(label_text, "a"):
                pass
        with open(label_text, "r+") as f:
            lines = f.readlines()
            for line in lines:
                words = line.split()
                self.__labels.appendRow(label_item(str(words[0])))

    def create_label(self, default=False):
        if default:
            return self.__unlabeled.get_label()

        ret = []
        dia = pick_label_dialog(self.__labels, ret)
        dia.exec_()
        if not len(ret):
            return self.__unlabeled.get_label()
        if isinstance(ret[0], str):
            ret[0] = self.__save_label(ret[0])
        r = ret[0].get_label()
        return r

    def create_label_2(self, name):
        if name == "":
            return None
        i = 0
        while self.__labels.item(i):
            if self.__labels.item(i).text() == name:
                return None
            i += 1
        li = label_item(name)
        self.__labels.appendRow(li)
        li.setCheckState(Qt.Checked)
        with open(self.__config.working_index_path() + "labels.txt", "a") as f:
            f.write(name + "\n")
        return li

    def __save_label(self, name):
        if name == "":
            return self.__unlabeled
        i = 0
        while self.__labels.item(i):
            if self.__labels.item(i).text() == name:
                return self.__labels.item(i)
            i += 1
        li = label_item(name)
        self.__labels.appendRow(li)
        with open(self.__config.working_index_path() + "labels.txt", "a") as f:
            f.write(name + "\n")
        return li

    def get_labels(self):
        return self.__labels

    def get_label(self, name):
        i = 0
        while self.__labels.item(i):
            if self.__labels.item(i).text() == name:
                return self.__labels.item(i).get_label()
            i += 1


