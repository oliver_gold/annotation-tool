from src.dialogs import enter_id_dialog
from time import time


class evaluation():
    def __init__(self, path):
        self.path = path

        self.user = []
        dia = enter_id_dialog(self.user)
        dia.exec_()

        self.checkpoint = time()
        self.log("starting with user " + self.user[0])

    def log(self, text):
        timestamp = time()
        edited = timestamp - self.checkpoint
        line = str(edited) + " " + text + "\n"
        with open(self.path + self.user[0] + ".txt", "a") as f:
            f.write(line)
