import sys

# qt libs
from PySide2.QtWidgets import QApplication, QMainWindow, QButtonGroup, QShortcut
from PySide2.QtGui import QPixmap, QStandardItemModel, QIcon, QKeySequence
from PySide2.QtCore import Qt, QSize

# generated ui
from gui.ui_mw import Ui_MainWindow

# numpy conversion lib
import numpy as np
import qimage2ndarray as i2a

from skimage.morphology import flood, binary_dilation
from skimage.util import img_as_bool, img_as_ubyte
from skimage.io import imsave

# own classes
from src.canvas import canvas
from src.mask_item import mask_item, mask_type, id_manager
from src.image_manager import image_manager, save_to_txt, remove_mask, save_label_txt
from src.config import config
from src.evaluation import evaluation

from src.dialogs import question_dialog, text_notification, edit_dialog
from src.colors import colors
from src.label_manager import label_manager


class MainWindow(QMainWindow):
    def __set_icons(self):
        cursor = QIcon()
        polyline = QIcon()
        polygon = QIcon()
        prev = QIcon()
        next = QIcon()
        add = QIcon()
        delete = QIcon()
        merge = QIcon()
        ok = QIcon()
        edit = QIcon()
        divide = QIcon()

        cursor.addPixmap(QPixmap(self.configs.icon_path() + "noun_Cursor.png"), QIcon.Normal, QIcon.Off)
        polyline.addPixmap(QPixmap(self.configs.icon_path() + "noun_path.png"), QIcon.Normal, QIcon.Off)
        polygon.addPixmap(QPixmap(self.configs.icon_path() + "noun_polygon.png"), QIcon.Normal, QIcon.Off)
        prev.addPixmap(QPixmap(self.configs.icon_path() + "noun_Arrow.png"), QIcon.Normal, QIcon.Off)
        next.addPixmap(QPixmap(self.configs.icon_path() + "noun_Arrow_rot.png"), QIcon.Normal, QIcon.Off)
        add.addPixmap(QPixmap(self.configs.icon_path() + "noun_add.png"), QIcon.Normal, QIcon.Off)
        delete.addPixmap(QPixmap(self.configs.icon_path() + "noun_Delete.png"), QIcon.Normal, QIcon.Off)
        merge.addPixmap(QPixmap(self.configs.icon_path() + "noun_Merge.png"), QIcon.Normal, QIcon.Off)
        ok.addPixmap(QPixmap(self.configs.icon_path() + "noun_Save.png"), QIcon.Normal, QIcon.Off)
        edit.addPixmap(QPixmap(self.configs.icon_path() + "noun_edit.png"), QIcon.Normal, QIcon.Off)
        divide.addPixmap(QPixmap(self.configs.icon_path() + "noun_divide.png"), QIcon.Normal, QIcon.Off)

        self.ui.checkBox_select.setIcon(cursor)
        self.ui.checkBox_poly_line.setIcon(polyline)
        self.ui.checkBox_polygon.setIcon(polygon)

        self.ui.pushButton_previous.setIcon(prev)
        self.ui.pushButton_submit.setIcon(ok)
        self.ui.pushButton_next.setIcon(next)

        self.ui.pushButton_add.setIcon(add)
        self.ui.pushButton_delete.setIcon(delete)
        self.ui.pushButton_merge.setIcon(merge)
        self.ui.pushButton_divide.setIcon(divide)
        self.ui.pushButton_edit_item.setIcon(edit)

    def __connect_buttons(self):
        self.ui.pushButton_next.clicked.connect(self.show_next_image)
        self.ui.pushButton_next.setToolTip("shows the next image and corresponding masks")
        self.shortcut_next = QShortcut(QKeySequence("n"), self.ui.pushButton_next)
        self.shortcut_next.activated.connect(self.show_next_image)

        self.ui.pushButton_submit.clicked.connect(self.__write_result)
        self.ui.pushButton_submit.setToolTip(
            "click to save a single checked mask, or if none is checked the last one, if it contains a label"
            "click to save a single checked mask, or if none is checked the last one, if it contains a label"
        )
        self.shortcut_submit = QShortcut(QKeySequence("o"), self.ui.pushButton_submit)
        self.shortcut_submit.activated.connect(self.__write_result)

        self.ui.pushButton_previous.clicked.connect(self.show_previous_image)
        self.ui.pushButton_previous.setToolTip("shows the previous image and corresponding masks")
        self.shortcut_previous = QShortcut(QKeySequence("b"), self.ui.pushButton_previous)
        self.shortcut_previous.activated.connect(self.show_previous_image)

        self.ui.pushButton_divide.setCheckable(True)
        self.ui.pushButton_divide.clicked.connect(self.__activate_poly_line_box)
        self.ui.pushButton_divide.setToolTip(
            "set at least two points in poly_line style to divide a single net_output mask along the resulting lines")
        self.shortcut_divide = QShortcut(QKeySequence("d"), self.ui.pushButton_divide)
        self.shortcut_divide.activated.connect(self.__activate_poly_line_box_shortcut)

        self.ui.pushButton_add.clicked.connect(self.__add)
        self.ui.pushButton_add.setToolTip("click to finish the current edited poly_line or polygon")
        self.shortcut_add = QShortcut(QKeySequence("a"), self.ui.pushButton_add)
        self.shortcut_add.activated.connect(self.__add)

        self.ui.pushButton_merge.clicked.connect(self.__merge)
        self.ui.pushButton_merge.setToolTip("click to generate a new binary mask out of all checked binary masks")
        self.shortcut_merge = QShortcut(QKeySequence("m"), self.ui.pushButton_merge)
        self.shortcut_merge.activated.connect(self.__merge)

        self.ui.pushButton_delete.clicked.connect(self.__delete)
        self.ui.pushButton_delete.setToolTip("click to delete all checked masks")
        self.shortcut_delete = QShortcut(QKeySequence.Delete, self.ui.pushButton_delete)
        self.shortcut_delete.activated.connect(self.__delete)

        self.ui.label.mousePressEvent = self.mouse
        # self.ui.label.mouseMoveEvent = self.mouse_move
        # self.ui.label.mouseReleaseEvent = self.mouse_release

        self.ui.pushButton_unselect_all.clicked.connect(self.__unselect_all)
        self.ui.pushButton_select_all.clicked.connect(self.__select_all)

        self.buttongroup.addButton(self.ui.checkBox_select)
        self.ui.checkBox_select.clicked.connect(self.__activate_select_box)
        self.ui.checkBox_select.setToolTip(
            "select tool: usable to pick superpixels or binary mask segments for generation of a new mask")
        self.shortcut_select = QShortcut(QKeySequence("s"), self.ui.checkBox_select)
        self.shortcut_select.activated.connect(self.__activate_select_box)
        self.ui.checkBox_select.setChecked(True)

        self.buttongroup.addButton(self.ui.checkBox_poly_line)
        self.ui.checkBox_poly_line.clicked.connect(self.__uncheck_divide_button)
        self.ui.checkBox_poly_line.setToolTip(
            "poly_line tool: click generates new vertex, that will be connected to the previous one, "
            "finish by clicking add button on top right corner\n"
            "with checked divide button instead of generating a line for e.g. outline annotation, it divides segments "
            "in a net_output mask")
        self.shortcut_poly_line = QShortcut(QKeySequence("l"), self.ui.checkBox_poly_line)
        self.shortcut_poly_line.activated.connect(self.__activate_poly_line_box)

        self.buttongroup.addButton(self.ui.checkBox_polygon)
        self.ui.checkBox_polygon.clicked.connect(self.__activate_polygon_box)
        self.ui.checkBox_polygon.setToolTip(
            "polygon tool: click generates new vertex, beginning from three vertices, the resulting polygon will be "
            "shown, finish by clicking add button on top right corner")
        self.shortcut_polygon = QShortcut(QKeySequence("p"), self.ui.checkBox_polygon)
        self.shortcut_polygon.activated.connect(self.__activate_polygon_box)

        self.ui.pushButton_edit_item.clicked.connect(self.__edit_item)
        self.ui.pushButton_edit_item.setToolTip(
            "click to edit the color of a single checked mask or (re)pick its' label"
        )
        self.shortcut_edit = QShortcut(QKeySequence("e"), self.ui.pushButton_edit_item)
        self.shortcut_edit.activated.connect(self.__edit_item)

    def __activate_polygon_box(self):
        self.ui.checkBox_polygon.setChecked(True)
        self.ui.pushButton_divide.setChecked(False)
        if self.evaluation_started:
            self.eval.log("activated polygon tool")

    def __activate_poly_line_box(self):
        self.ui.checkBox_poly_line.setChecked(True)
        if self.ui.pushButton_divide.isChecked():
            if self.evaluation_started:
                self.eval.log("checked divide button")
        else:
            if self.evaluation_started:
                self.eval.log("un-checked divide button")

    def __activate_poly_line_box_shortcut(self):
        self.ui.checkBox_poly_line.setChecked(True)
        if self.ui.pushButton_divide.isChecked():
            self.ui.pushButton_divide.setChecked(False)
            if self.evaluation_started:
                self.eval.log("un-checked divide button")
        else:
            self.ui.pushButton_divide.setChecked(True)
            if self.evaluation_started:
                self.eval.log("checked divide button")

    def __activate_select_box(self):
        self.ui.checkBox_select.setChecked(True)
        self.ui.pushButton_divide.setChecked(False)
        if self.evaluation_started:
            self.eval.log("activated select tool")

    def __uncheck_divide_button(self):
        self.ui.pushButton_divide.setChecked(False)
        if self.evaluation_started:
            self.eval.log("clicked poly line tool")

    def __scale(self):
        # TODO
        w = self.configs.width()
        h = self.configs.height()
        s = w / h

        if s < 16 / 9:
            h = 900

    def __write_result(self):
        """
        saves the checked masks to the preferred output
        """
        masks = []
        i = 0
        while self.model.item(i):
            if self.model.item(i).get_mask_type() > mask_type.SEGMENT:
                if self.model.item(i).checkState():
                    masks.append(self.model.item(i))
            i += 1
        if len(masks) == 0:
            #dia = text_notification("please select the masks you want so save")
            #dia.exec_()
            last = None
            i = 0
            while self.model.item(i):
                last = self.model.item(i)
                i += 1
            masks.append(last)

        if len(masks) == 1:
            if masks[0].get_label() is None:
                return
            m = np.zeros(masks[0].get_mask().shape, dtype=np.uint8)
            m[masks[0].get_mask()] = 255

            name = self.configs.result_path() + masks[0].get_label().get_name() + "-" + self.manager.get_file_name(self.configs.active_pointer() - 1)
            imsave(name, m)

            dia = text_notification("mask was saved to " + name)
            dia.exec_()
        elif len(masks) > 1:
            for mask in masks:
                if mask.get_label() is None:
                    continue
                m = np.zeros(mask.get_mask().shape, dtype=np.uint8)
                m[mask.get_mask()] = 255

                name = self.configs.result_path() + mask.get_label().get_name() + "-" + self.manager.get_file_name(
                    self.configs.active_pointer() - 1)
                imsave(name, m)
            dia = text_notification("saved multiple masks")
            dia.exec_()
        if self.evaluation_started:
            m_ids = []
            for m in masks:
                m_ids.append(m.get_id())
            self.eval.log("wrote out masks " + str(m_ids))

    def __select_all(self):
        i = 0
        while self.model.item(i):
            self.model.item(i).setCheckState(Qt.Checked)
            i += 1
        if self.evaluation_started:
            self.eval.log("selected all")

    def __unselect_all(self):
        i = 0
        while self.model.item(i):
            self.model.item(i).setCheckState(Qt.Unchecked)
            i += 1
        if self.evaluation_started:
            self.eval.log("un-selected all")

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """
        ui interactions
        """
        self.buttongroup = QButtonGroup()
        self.__connect_buttons()

        # list view on the left, containing masks TODO implement model containing methods
        self.model = QStandardItemModel(self.ui.listView)
        self.model.itemChanged.connect(self.on_item_changed)
        self.ui.listView.setModel(self.model)

        """
        background loading and processing
        """
        self.configs = config()
        self.colors = colors()

        self.__scale()

        w = self.configs.width()
        h = self.configs.height()
        self.ui.label.setMinimumSize(QSize(w, h))
        self.ui.label.setMaximumSize(QSize(w, h))

        self.__set_icons()
        idm = []
        self.lm = label_manager(self.configs)
        self.manager = image_manager(self.configs, self.colors, self.lm, None, idm)
        self.__id_manager = None
        if len(idm) == 1:
            self.__id_manager = idm[0]
            self.manager.preload_images()
        image_data = self.manager.fetch_image_startup(self.__id_manager)
        if self.__id_manager is None:
            self.__id_manager = self.manager.get_id_manager()
            self.manager.preload_images()

        self.__set_arrow_button_activity()

        self.ui.label.setPixmap(QPixmap.fromImage(i2a.array2qimage(image_data[0], True)))
        for m in image_data[1]:
            self.model.appendRow(m)

        self.canvas = canvas(image_data[0], self.configs.resolution())

        self.polygon = None
        self.poly_line = None
        self.marked_bin_segments = None

        """
        label test
        """
        self.keyPressEvent = self.__key_event

        self.eval = None
        self.evaluation_started = False
        self.saved = False

    def __key_event(self, key):
        if key.key() == Qt.Key_Escape:
            if self.ui.checkBox_poly_line.isChecked():
                self.__delete_poly_line()
            elif self.ui.checkBox_polygon.isChecked():
                self.__delete_polygon()
            elif self.ui.checkBox_select.isChecked():
                self.__delete_marked_bin_segments()

            if self.evaluation_started:
                self.eval.log("cancelled currently edited item")

    def __edit_item(self):
        i = 0
        checked = []
        while self.model.item(i):
            if self.model.item(i).checkState() == Qt.Checked:
                checked.append(self.model.item(i))
            i += 1
        if len(checked) != 1:
            return
        dia = edit_dialog(checked[0], self.lm, self.colors.get_color_model(), self.configs)
        ret = dia.exec_()
        if self.evaluation_started:
            self.eval.log("clicked edit for one mask")
        if ret:
            if checked[0].get_label() is not None:
                save_label_txt(self.configs.working_index_path(), self.configs.active_pointer() - 1, checked[0])
                self.canvas.recompute()

                if self.evaluation_started:
                    self.eval.log("new label: " + checked[0].get_label().get_name())
        else:
            if self.evaluation_started:
                self.eval.log("cancelled editing")

    def __delete_polygon(self):
        if self.polygon is not None:
            self.canvas.unregister_mask(self.polygon)
            self.polygon = None
            self.canvas.recompute()
            img = self.canvas.get_result_image()
            self.ui.label.setPixmap(QPixmap.fromImage(img))

    def __delete_poly_line(self):
        if self.poly_line is not None:
            self.canvas.unregister_mask(self.poly_line)
            self.poly_line = None
            self.canvas.recompute()
            img = self.canvas.get_result_image()
            self.ui.label.setPixmap(QPixmap.fromImage(img))
            if self.ui.pushButton_divide.isChecked():
                self.ui.pushButton_divide.setChecked(False)

    def __delete_marked_bin_segments(self):
        if self.marked_bin_segments is not None:
            self.canvas.unregister_mask(self.marked_bin_segments)
            self.marked_bin_segments = None
            self.canvas.recompute()
            img = self.canvas.get_result_image()
            self.ui.label.setPixmap(QPixmap.fromImage(img))

    def on_item_changed(self, item):
        """
        called when a mask in the list is checked or renamed
        adds or removes masks from the active masks list
        :param item:
        :return:
        """
        if item.checkState():
            self.canvas.register_mask(item)
            if self.evaluation_started:
                self.eval.log("checked mask " + str(item.get_id()))
        else:
            self.canvas.unregister_mask(item)
            if self.evaluation_started:
                self.eval.log("un-checked mask " + str(item.get_id()))
        self.show_result()

    def show_result(self):
        """
        sets the image that is shown to the user,
        must be called whenever the user interacts with the shown image
        :return: True if successful
        """
        img = self.canvas.get_result_image()
        if img is not None:
            self.ui.label.setPixmap(QPixmap.fromImage(img))
            return True
        return False

    def mouse(self, event):
        x = event.pos().x()
        y = event.pos().y()
        
        if self.evaluation_started:
            self.eval.log("clicked x:" + str(x) + " y: " + str(y))

        if self.ui.checkBox_polygon.isChecked():
            self.__polygon(x, y)
        elif self.ui.checkBox_select.isChecked():
            if self.canvas.superpixels_active():
                self.canvas.update_superpixel_segments(x, y)
            else:
                bm = self.__check_single_mask()
                if bm is None:
                    return
                if bm.get_mask()[y, x] == False:
                    return
                self.__mark_bin_segments(bm, (y, x))

        elif self.ui.checkBox_poly_line.isChecked():
            self.__poly_line(x, y)

        img = self.canvas.get_result_image()
        if img is not None:
            self.ui.label.setPixmap(QPixmap.fromImage(img))

    def __mark_bin_segments(self, bm, pos):
        if self.marked_bin_segments is None:
            self.marked_bin_segments = mask_item(self.__id_manager.get_id(), active_bin_seg=True)
            self.marked_bin_segments.setCheckState(Qt.Checked)
            self.marked_bin_segments.setBackground(self.colors.get_color())

            m = bm.get_mask()
            m = img_as_ubyte(m)
            a = flood(m, pos)
            a = img_as_bool(a)
            self.marked_bin_segments.set_binary_mask(a)

            self.canvas.register_mask(self.marked_bin_segments)
        else:
            m = bm.get_mask()
            m = img_as_ubyte(m)
            a = flood(m, pos)
            a = img_as_bool(a)

            mask = self.marked_bin_segments.get_mask()
            if mask[pos] == True:
                mask[a] = False
            else:
                mask[a] = True
        self.canvas.recompute()
        self.canvas.recompute()

    def __check_single_mask(self, m_type=mask_type.NET_OUTPUT):
        """
        help function that checks if the only active mask is of specified mask_type
        :return: mask_item with specified mask_type or None
        """
        i = 0
        ret = []
        while self.model.item(i):
            if self.model.item(i).checkState() == Qt.Checked:
                ret.append(self.model.item(i))
            i += 1
        if len(ret) == 1 and ret[0].get_mask_type() == m_type:
            return ret[0]
        return None

    def __add(self):
        if self.ui.checkBox_poly_line.isChecked():
            if self.ui.pushButton_divide.isChecked():
                m = self.__check_single_mask()
                if m is None:
                    m = self.__check_single_mask(mask_type.BINARY)
                    if m is None:
                        dia = text_notification("you can only divide a single net_output or binary mask")
                        dia.exec_()
                        self.__delete_poly_line()
                    return
                img = m.get_mask()
                pl = self.poly_line.get_mask()
                pl = binary_dilation(pl)
                mask = np.logical_and(img, pl)
                img[mask] = False
                # m.overwrite_mask()
                self.__delete_poly_line()
                self.ui.pushButton_divide.setChecked(False)
            else:
                dia = edit_dialog(self.poly_line, self.lm, self.colors.get_color_model())
                dia.exec_()
                self.__append_save_item(self.poly_line)
            self.poly_line = None

        elif self.ui.checkBox_polygon.isChecked():
            #net_mask = self.__check_single_mask()
            #if net_mask is None:
                dia = edit_dialog(self.polygon, self.lm, self.colors.get_color_model())
                dia.exec_()
                self.__append_save_item(self.polygon)
                self.polygon = None
            #else:
                """
                generates a new mask of the intersection of the polygon and a single net mask
                """
                """result = np.logical_and(self.polygon.get_mask(), net_mask.get_mask())
                m = mask_item(self.__id_manager.get_id())
                m.set_binary_mask(result)
                m.setBackground(self.colors.get_color())
                m.set_label(self.lm.create_label())
                self.__append_save_item(m)
                self.canvas.unregister_mask(self.polygon)
                self.polygon = None"""
        else:
            if self.canvas.superpixels_active():
                self.__merge_superpixels()
            elif self.marked_bin_segments is not None:
                dia = edit_dialog(self.marked_bin_segments, self.lm, self.colors.get_color_model())
                dia.exec_()
                self.__append_save_item(self.marked_bin_segments)
                self.marked_bin_segments = None
        if self.evaluation_started:
            self.eval.log("added current item")
        self.canvas.recompute()
        self.ui.label.setPixmap(QPixmap.fromImage(self.canvas.get_result_image()))

    def __polygon(self, x, y):
        rad = 4
        if self.polygon is None:
            self.polygon = mask_item(self.__id_manager.get_id())
            self.polygon.setCheckState(Qt.Checked)
            self.polygon.setBackground(self.colors.get_color())
            self.polygon.set_polygon_mask([x], [y], self.configs.resolution())
            self.canvas.register_mask(self.polygon)
            self.canvas.recompute()
        else:
            self.polygon.add_polygon_point(x, y)
            self.polygon.compute_polygon_mask()
        self.canvas.recompute()
        return False

    def __poly_line(self, x, y):
        rad = 4
        if self.poly_line is None:
            self.poly_line = mask_item(self.__id_manager.get_id())
            self.poly_line.setCheckState(Qt.Checked)
            self.poly_line.setBackground(self.colors.get_color())
            self.poly_line.set_poly_line_mask([x], [y], self.configs.resolution())
            self.canvas.register_mask(self.poly_line)
            self.canvas.recompute()
        else:
            self.poly_line.add_poly_line_point(x, y)
            self.poly_line.compute_poly_line_mask()
        self.canvas.recompute()

    def __update_model(self, tmp2):
        self.model.clear()
        self.canvas.clear()
        self.ui.label.setPixmap(QPixmap.fromImage(i2a.array2qimage(tmp2[0], True)))
        self.canvas.update_image(tmp2[0])

        for i in tmp2[1]:
            self.model.appendRow(i)

    def __buffer_items(self):
        tmp = []
        i = 0
        while self.model.item(i):
            tmp.append(self.model.item(i))
            self.model.item(i).setCheckState(Qt.Unchecked)
            i += 1
        return tmp

    def show_next_image(self):
        self.__write_result()
        tmp = self.__buffer_items()
        tmp2 = self.manager.fetch_next_image(tmp)
        self.__update_model(tmp2)
        self.__set_arrow_button_activity()

        if self.eval is None:
            self.eval = evaluation(self.configs.result_path())
            self.evaluation_started = True
        else:
            self.eval.log("next image")

    def show_previous_image(self):
        if self.configs.active_pointer() == 1:
            print("already first image")
            return
        tmp = self.__buffer_items()
        tmp2 = self.manager.fetch_prev_image(tmp)
        self.__update_model(tmp2)
        self.__set_arrow_button_activity()

        if self.evaluation_started:
            self.eval.log("previous image")

    def __set_arrow_button_activity(self):
        if self.configs.active_pointer() == 1:
            self.ui.pushButton_previous.setEnabled(False)
        else:
            self.ui.pushButton_previous.setEnabled(True)
        if self.configs.active_pointer() == self.manager.get_image_number():
            self.ui.pushButton_next.setEnabled(False)
        else:
            self.ui.pushButton_next.setEnabled(True)

    def __delete(self):
        self.__delete_masks()

    def __delete_masks(self, dialog=True):
        if dialog:
            dia = question_dialog()
            ret = dia.exec_()
        else:
            ret = True
        if ret:
            del_ids = []
            i = 0
            while self.model.item(i):
                if self.model.item(i).checkState():
                    if self.model.item(i).get_mask_type() == mask_type.NET_OUTPUT:
                        print("didn't delete the net mask")
                    else:
                        m = self.model.item(i)
                        del_ids.append(m.get_id())
                        m.setCheckState(Qt.Unchecked)
                        self.model.takeItem(i)
                        self.model.takeRow(i)
                        remove_mask(self.configs.working_index_path(), str(self.configs.active_pointer() - 1), m)
                        continue
                i += 1
            if self.evaluation_started:
                self.eval.log("deleted masks " + str(del_ids))
            self.canvas.recompute()
        return ret

    def __merge(self):
        self.__merge_masks()

    def __merge_masks(self):
        checked = []
        i = 0
        while self.model.item(i):
            if self.model.item(i).checkState():
                if self.model.item(i).get_mask_type() > mask_type.SEGMENT:
                    checked.append(i)
            i += 1
        if len(checked) < 2:
            return
        result = np.zeros(self.configs.resolution(), dtype=bool)
        for i in checked:
            result = np.logical_or(result, self.model.item(i).get_mask())
        self.__delete_masks(False)
        ret = mask_item(self.__id_manager.get_id())
        ret.setBackground(self.colors.get_color())
        dia = edit_dialog(ret, self.lm, self.colors.get_color_model())
        dia.exec_()
        ret.set_binary_mask(result)
        self.__append_save_item(ret)

        if self.evaluation_started:
            self.eval.log("merged masks")

    def __merge_superpixels(self):
        """
        generates a mask from every selected superpixel
        :return:
        """
        if len(self.canvas.get_superpixel_mask_item().get_active_segments()) == 0:
            print("cannot merge zero elements")
            return

        m = mask_item(self.__id_manager.get_id())
        m.setBackground(self.colors.get_color())

        m.set_binary_mask(self.canvas.get_superpixel_mask_item().get_active_segments_mask())

        dia = edit_dialog(m, self.lm, self.colors.get_color_model())
        dia.exec_()
        self.canvas.update_superpixel_segments(0, 0, True)
        self.ui.label.setPixmap(QPixmap.fromImage(self.canvas.get_result_image()))

        self.__append_save_item(m)

    def __append_save_item(self, item):
        # item.setText(self.manager.get_file_name(self.configs.active_pointer()) + item.text())
        item.save(self.configs.working_image_path())
        save_to_txt(self.configs.working_index_path(), str(self.configs.active_pointer() - 1), item)
        self.model.appendRow(item)


if __name__ == '__main__':
    # Create the Qt Application
    app = QApplication(sys.argv)
    # Create and show the form
    mw = MainWindow()
    # mw.resize(1920, 1000)
    mw.show()
    # Run the main Qt loop
    sys.exit(app.exec_())
