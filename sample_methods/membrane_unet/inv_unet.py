import subprocess
import shlex
import os
import shutil
from skimage.io import imread, imsave


def wrap_invocation(img):
    if os.path.exists("tmp"):
        shutil.rmtree("tmp")
    os.mkdir("tmp")
    imsave("tmp/i.png", img)
    run(image_path="tmp/i.png", result_path="tmp/o.png")

    loaded = imread("tmp/o.png")
    shutil.rmtree("tmp")
    return loaded


def run(image_path, result_path, weights_path=None,
        script_path="sample_methods/membrane_unet/run_unet.py",):
    s = "python3 "
    s += script_path
    s += " -i "
    s += image_path
    s += " --result "
    s += result_path
    if weights_path is not None:
        s += " --weights "
        s += weights_path
    print("trying to run membrane unet:\n" + str(s))
    args = shlex.split(s)
    subprocess.call(args)


if __name__ == '__main__':
    wrap_invocation("data/input_data/x1/frame000.png")
