from sample_methods.membrane_unet.model import unet
from sample_methods.membrane_unet.data import one_test_gen

from skimage.io import imsave, imread

import argparse


def run_unet(path, img):
    model = unet(path + "unet_membrane.hdf5")

    test_gen = one_test_gen(img)
    results = model.predict_generator(test_gen, 1, verbose=0)

    i = results[0]
    i = i[:, :, 0]
    return i


def run_unet_2(model, img):
    test_gen = one_test_gen(img)
    results = model.predict_generator(test_gen, 1, verbose=0)

    i = results[0]
    i = i[:, :, 0]
    return i


def run_unet_3(image, result, weights):
    model = unet(weights)
    img = imread(image)
    img = img[:, :, 0]
    test_gen = one_test_gen(img)
    res = model.predict_generator(test_gen, 1, verbose=0)
    i = res[0]
    i = i[:, :, 0]
    imsave(result, i)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--image", type=str, help="path to image to be processed")
    parser.add_argument("-r", "--result", type=str, help="path the result shall be saved to")
    parser.add_argument("-w", "--weights", type=str, default="sample_methods/membrane_unet/unet_membrane.hdf5", help="path to weights file")

    args = parser.parse_args()
    print(args.image, args.result, args.weights)

    run_unet_3(args.image, args.result, args.weights)
