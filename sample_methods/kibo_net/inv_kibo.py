import subprocess
import shlex
import os
import shutil
from skimage.io import imread


def wrap_invocation(input_image_path, input_image_name):
    if os.path.exists("tmp"):
        shutil.rmtree("tmp")
    os.mkdir("tmp")
    os.mkdir("tmp/x1")
    os.mkdir("tmp/result")
    shutil.copyfile(input_image_path + input_image_name, "tmp/x1/" + input_image_name)
    run(data_path="tmp", result_path="tmp/result")

    loaded = imread("tmp/result/x1/" + input_image_name)
    # loaded = imread("data/result_frame000.png")
    shutil.rmtree("tmp")
    return loaded


def run(script_path="sample_methods/kibo_net/train.py",
        data_path="data/input_data", result_path="data/results",
        weights_path="sample_methods/kibo_net/network_weights_16_all_data_sets.hdf5"):

    s = "python3.5 "
    s += script_path
    s += " -m test -dtest "
    s += data_path
    s += " --test_sets 1 --test_save "
    s += result_path
    s += " --weights "
    s += weights_path
    print("running kibo-net:\n" + str(s))
    args = shlex.split(s)
    subprocess.call(args)


if __name__ == '__main__':
    run()
