from skimage.segmentation import slic


def calculate_superpixel(img, mask_model, colors, n_seg=300, sigm=5):
    spx = slic(img, n_segments=n_seg, compactness=.25, sigma=sigm, multichannel=False)
    spx = spx[:, :, 0]

    mask_model.set_segment_mask(spx)
    mask_model.setBackground(colors.get_color())
    mask_model.setText(str(n_seg) + "-superpixel-sigma-" + str(sigm))