# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'firsttry.ui',
# licensing of 'firsttry.ui' applies.
#
# Created: Mon Jul  1 16:13:22 2019
#      by: pyside2-uic  running on PySide2 5.13.0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets
from methods.superpixel import superpixel as spx
from skimage.segmentation import mark_boundaries
from skimage import io
from skimage.util import img_as_float

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(2196, 1315)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMaximumSize(QtCore.QSize(2196, 1315))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setMaximumSize(QtCore.QSize(1920, 1080))
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.radioButton = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton.setObjectName("radioButton")
        self.horizontalLayout_2.addWidget(self.radioButton)
        self.radioButton_2 = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton_2.setObjectName("radioButton_2")
        self.horizontalLayout_2.addWidget(self.radioButton_2)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setCheckable(True)
        self.pushButton_2.setChecked(False)
        self.pushButton_2.setObjectName("pushButton_2")
        self.horizontalLayout_2.addWidget(self.pushButton_2)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_2.addWidget(self.pushButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.listView = QtWidgets.QListView(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.listView.sizePolicy().hasHeightForWidth())
        self.listView.setSizePolicy(sizePolicy)
        self.listView.setMinimumSize(QtCore.QSize(200, 0))
        self.listView.setMaximumSize(QtCore.QSize(200, 16777215))
        self.listView.setObjectName("listView")
        self.horizontalLayout.addWidget(self.listView)
        self.label = QtWidgets.QLabel(self.centralwidget)

        #add label for overlayed superpixels
        self.spx_label = QtWidgets.QLabel(self.centralwidget)

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap("stairs.jpg"))
        self.label.setObjectName("label")

        #setup label and environment for changing to spx overlayed image
        self.spx_done = False
        self.label.setSizePolicy(sizePolicy)
        self.label.setText("")
        self.label.setObjectName("label")

        #connect superpixel button
        self.pushButton_2.clicked.connect(self.show_superpixel)

        #old complicated syntax
        #QtCore.QObject.connect(self.pushButton_2, QtCore.SIGNAL('clicked()'), self.execute_superpixel(self.label))

        self.horizontalLayout.addWidget(self.label)
        self.verticalLayout.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 2196, 32))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "MainWindow", None, -1))
        self.radioButton.setText(QtWidgets.QApplication.translate("MainWindow", "ploygon", None, -1))
        self.radioButton_2.setText(QtWidgets.QApplication.translate("MainWindow", "select", None, -1))
        self.pushButton_2.setText(QtWidgets.QApplication.translate("MainWindow", "superpixel", None, -1))
        self.pushButton.setText(QtWidgets.QApplication.translate("MainWindow", "next", None, -1))


    def show_superpixel(self):

        if self.spx_done:
            print("calling toggle_label_image")
            self.toggle_label_image()

        #should be called once per image and after editing superpixel parameters TODO
        else:
        #saves the image for intermediate representation to disk
        #TODO remove this intermediate representation, use unabeled image instead
        #TODO also connected in toggle_label_image()

            print("cumputing images...")
            self.label.pixmap().save("label.png", "PNG")
            img = img_as_float(io.imread("label.png"))
            s = mark_boundaries(img, spx("label.png"))
            io.imsave("masked.png", s)
            print("now calling toggle_label_image")
            self.spx_done = True
            self.toggle_label_image()

    def toggle_label_image(self):

        if self.pushButton_2.isChecked():
            self.label.setPixmap(QtGui.QPixmap("masked.png"))
        else:
            self.label.setPixmap(QtGui.QPixmap("label.png"))
