# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'edit_dialog.ui',
# licensing of 'edit_dialog.ui' applies.
#
# Created: Fri Sep 20 12:19:40 2019
#      by: pyside2-uic  running on PySide2 5.13.1
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 299)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.verticalLayout.addLayout(self.verticalLayout_7)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_color = QtWidgets.QLabel(Dialog)
        self.label_color.setAlignment(QtCore.Qt.AlignCenter)
        self.label_color.setObjectName("label_color")
        self.verticalLayout_2.addWidget(self.label_color)
        self.listView_colors = QtWidgets.QListView(Dialog)
        self.listView_colors.setObjectName("listView_colors")
        self.verticalLayout_2.addWidget(self.listView_colors)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_labels = QtWidgets.QLabel(Dialog)
        self.label_labels.setAlignment(QtCore.Qt.AlignCenter)
        self.label_labels.setObjectName("label_labels")
        self.verticalLayout_5.addWidget(self.label_labels)
        self.listView_labels = QtWidgets.QListView(Dialog)
        self.listView_labels.setObjectName("listView_labels")
        self.verticalLayout_5.addWidget(self.listView_labels)
        self.horizontalLayout.addLayout(self.verticalLayout_5)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setObjectName("lineEdit")
        self.verticalLayout_4.addWidget(self.lineEdit)
        self.pushButton_create_label = QtWidgets.QPushButton(Dialog)
        self.pushButton_create_label.setObjectName("pushButton_create_label")
        self.verticalLayout_4.addWidget(self.pushButton_create_label)
        self.horizontalLayout.addLayout(self.verticalLayout_4)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pushButton_accept = QtWidgets.QPushButton(Dialog)
        self.pushButton_accept.setObjectName("pushButton_accept")
        self.horizontalLayout_2.addWidget(self.pushButton_accept)
        self.pushButton_cancel = QtWidgets.QPushButton(Dialog)
        self.pushButton_cancel.setObjectName("pushButton_cancel")
        self.horizontalLayout_2.addWidget(self.pushButton_cancel)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "edit mask properties", None, -1))
        self.label_color.setText(QtWidgets.QApplication.translate("Dialog", "pick color", None, -1))
        self.label_labels.setText(QtWidgets.QApplication.translate("Dialog", "pick label", None, -1))
        self.pushButton_create_label.setText(QtWidgets.QApplication.translate("Dialog", "create label", None, -1))
        self.pushButton_accept.setText(QtWidgets.QApplication.translate("Dialog", "accept", None, -1))
        self.pushButton_cancel.setText(QtWidgets.QApplication.translate("Dialog", "cancel", None, -1))

